
//
//  ArrProductosIncluidosBean.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 25/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class ArrProductosIncluidosBean: NSObject {
    var Id : String?
    var Nombre : String?
    var AgrupacionAddon: String?
    var CantidadDN : String?
    var CantidadTroncal : String?
    var Comentario : String?
    var EsAutomaticoCiudad : String?
    var EsCargoUnico : String?
    var Estatus : String?
    var EsVisible : String?
    var IdBrmArrear : String?
    var IdBrmCU : String?
    var IdBrmForward : String?
    var IEPS : String?
    var IVA : String?
    var MaximoAgregar : String?
    var NombreEditable : String?
    var PlanDescuentoId : String?
    var Plazo : String?
    var PrecioBase : String?
    var PrecioEditable : String?
    var PrecioProntoPago : String?
    var ProductoPadre : String?
    var TipoProducto : String?
    var IdProducto : String?
    var NameProducto : String?
    var ProductoId : String?
    var Ciudad : String?
    var VelocidadSubida : String?
    var VelocidadBajada : String?
    var TieneIPDinamica : String?
    var TieneIPFija : String?
    var TieneSTBAdicional : String?
    var EsCCTV : String?
    var EsWiFi : String?
    var Cantidad : String?
    var EstatusProducto : String?
    var FechaInicio : String?
    var FechaFin : String?
    var ComentarioProducto : String?
    var EsProntoPago : String?
}

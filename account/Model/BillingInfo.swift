//
//  BillingInfo.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 03/11/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

public class BillingInfo: NSObject, Mappable{
    var lastBillBalance: LastBillBalance?
    
    public required convenience  init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        lastBillBalance <- map["LastBillBalance"]
    }
    
}

//
//  RestFaultElementBean.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 24/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

class RestFaultElementBean: NSObject, Mappable {
    
    var summary : String = ""
    var code : String = ""
    var detail : String = ""
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        summary		<- map["summary"]
        code		<- map["code"]
        detail		<- map["detail"]
    }

}

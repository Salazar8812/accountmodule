//
//  MyAccountViewPresenter.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 19/09/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

protocol MyAccountDelegate : NSObjectProtocol {
    
    func onSuccessLoadMyAccount(myAccountResponse : MyAccountResponse)
    
    func onSuccesCheckBalanceBRM(checkBRMRespnse: CheckBalanceBRMResponse)
    
}

class MyAccountViewPresenter: BaseEstrategiaPresenter {
    
    var mStatements : [Statements] = []
    
    var mMyAccountDelegate : MyAccountDelegate!
    
    init(viewController: BaseViewController, myAccountDelegate : MyAccountDelegate) {
        super.init(viewController: viewController)
        self.mMyAccountDelegate = myAccountDelegate
    }
    
    func loadInfoMyAccount(){
        let requestModel : MyAccountRequest = MyAccountRequest(accountNumber: mUser.accountNumber)
        RetrofitManager<MyAccountResponse>.init(requestUrl: ApiDefinition.WS_MY_ACCOUNT, delegate: self).request(requestModel: requestModel)
    }
    
    
    func getBalanceBRM(){
        let requestModel : CheckBalanceBRMRequest = CheckBalanceBRMRequest(accountNumber: mUser.accountNumber)
        RetrofitManager<CheckBalanceBRMResponse>.init(requestUrl: ApiDefinition.WS_CHECK_BALANCE_BRM
            , delegate: self).request(requestModel: requestModel)
    }
    
    func successLoadMyAccount(requestUrl : String, myAccountResponse : MyAccountResponse){
        if myAccountResponse.result == "0"{
            mMyAccountDelegate.onSuccessLoadMyAccount(myAccountResponse: myAccountResponse)
            
            let requestModel : StatementRequest = StatementRequest(accountNumber: mUser.accountNumber)
            RetrofitManager<StatementResponse>.init(requestUrl: ApiDefinition.WS_STATEMENTS, delegate: self).request(requestModel: requestModel)
        } else {
            onErrorLoadResponse(requestUrl : requestUrl, messageError : "")
        }
    }
    
    func successLoadStatements(requestUrl : String, statementsResponse : StatementResponse){
        if statementsResponse.result == "0"{
            mStatements = statementsResponse.services
            AlertDialog.hideOverlay()
        } else {
            onErrorLoadResponse(requestUrl : requestUrl, messageError : "")
        }
    }
    
    func showStatements(){

        let optionMenu : UIAlertController = UIAlertController(title: nil, message: "Selecciona el mes", preferredStyle: .actionSheet)
        
        var service : Statements!
        var dateString = ""
        
        if mStatements.indices.contains(0) && mStatements[0].date != "" {
            dateString = ""
            service = self.mStatements[0]
            if service.date != "" {
                let date = DateUtils.toDate(date: service.date)
                dateString = "\(date.monthMedium) \(date.year)"
            }
            let monthOne = UIAlertAction(title: dateString, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                service = self.mStatements[0]
                self.showStatePayment(urlStatement: service.url)
            })
            optionMenu.addAction(monthOne)
        }
        
        if mStatements.indices.contains(1) && mStatements[1].date != ""{
            dateString = ""
            service = self.mStatements[1]
            if service.date != "" {
                let date = DateUtils.toDate(date: service.date)
                dateString = "\(date.monthMedium) \(date.year)"
            }
            let monthTwo = UIAlertAction(title: dateString, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                service = self.mStatements[1]
                self.showStatePayment(urlStatement: service.url)
            })
            optionMenu.addAction(monthTwo)
        }
        
        if mStatements.indices.contains(2) && mStatements[2].date != "" {
            dateString = ""
            service = self.mStatements[2]
            if service.date != "" {
                let date = DateUtils.toDate(date: service.date)
                dateString = "\(date.monthMedium) \(date.year)"
            }
            let monthThree = UIAlertAction(title: dateString, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                service = self.mStatements[2]
                self.showStatePayment(urlStatement: service.url)
            })
            optionMenu.addAction(monthThree)
        }
        
        /*optionMenu.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
        
        mViewController.present(optionMenu, animated: true, completion: nil)*/
        
        optionMenu.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
        
        optionMenu.popoverPresentationController?.sourceView = mViewController.view;
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x: mViewController.view.bounds.midX, y: mViewController.view.bounds.midY, width: 0, height: 0)

        mViewController.present(optionMenu, animated: true, completion: nil)
    }
    
    func showStatePayment(urlStatement : String){
        let url = URL(string: urlStatement)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    override func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        if requestUrl == ApiDefinition.WS_MY_ACCOUNT {
            successLoadMyAccount(requestUrl: requestUrl, myAccountResponse: response as! MyAccountResponse)
        } else if requestUrl == ApiDefinition.WS_STATEMENTS {
            successLoadStatements(requestUrl: requestUrl, statementsResponse: response as! StatementResponse)
        } else if requestUrl == ApiDefinition.WS_CHECK_BALANCE_BRM {
            onSuccesCheckBalanceBRM(checkBRM: response as! CheckBalanceBRMResponse)
        }
    }
    
    func onSuccesCheckBalanceBRM(checkBRM: CheckBalanceBRMResponse){
        mMyAccountDelegate.onSuccesCheckBalanceBRM(checkBRMRespnse: checkBRM)
    }
    override func onErrorLoadResponse(requestUrl : String, messageError : String){
        AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_intern, view: mViewController)
        AlertDialog.hideOverlay()
    }

}

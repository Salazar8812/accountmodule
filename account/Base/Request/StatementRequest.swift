//
//  StatementRequest.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 14/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

class StatementRequest: BaseRequest {
    
    var accountNumber : String?
    
    init(accountNumber : String) {
        super.init()
        self.accountNumber = accountNumber
    }
    
    public required init?(map: Map) {
        super.init()
    }
    
    internal override func mapping(map : Map){
        super.mapping(map: map)
        accountNumber <- map["AccountNumber"]
    }

}

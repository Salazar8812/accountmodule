//
//  MyAccountRequest.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 09/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

class MyAccountRequest: BaseRequest {
    
    var accountNumber : String?
    
    init(accountNumber : String) {
        super.init()
        self.accountNumber = accountNumber
    }
    
    public required init?(map: Map) {
        super.init()
    }
    
    internal override func mapping(map : Map){
        super.mapping(map: map)
        accountNumber <- map["AccountNumber"]
    }

}

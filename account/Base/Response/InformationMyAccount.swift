//
//  InformationMyAccount.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 09/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

class InformationMyAccount: BaseResponse {

    var titulate : String = ""
    var accountNumber : String = ""
    var contractedPackage : String = ""
    var packageSpeed : String = ""
    var promotion : String = ""
    var balance : String = ""
    var promotionMonth : String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        self.titulate     <- map["Titular"]
        self.accountNumber     <- map["AccountNumber"]
        self.contractedPackage     <- map["ContractedPackage"]
        self.packageSpeed     <- map["PacketSpeed"]
        self.promotion     <- map["Promotion"]
        self.balance     <- map["Balance"]
        self.promotionMonth     <- map["PromocionXMes"]
    }
}

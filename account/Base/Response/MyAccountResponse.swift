//
//  MyAccountResponse.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 09/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

class MyAccountResponse: BaseResponse {
    
    var information : InformationMyAccount!
    
    required convenience public init?(map: Map) {
        self.init()
        information = InformationMyAccount()
    }

    override func mapping(map: Map) {
        super.mapping(map: map)
        self.information     <- map["Information"]
    }
    
}

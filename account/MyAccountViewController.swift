//
//  MyAccountViewController.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 03/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import CoreLocation

class MyAccountViewController: BaseViewController, MyAccountDelegate {

    @IBOutlet weak var mNameTitulateLabel: UILabel!
    @IBOutlet weak var mAccountNumberLabel: UILabel!
    @IBOutlet weak var mPackageNameLabel: UILabel!
    @IBOutlet weak var mVelocityLabel: UILabel!
    @IBOutlet weak var mPaymentAmountLabel: UILabel!
    var mMyAccountPresenter : MyAccountViewPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        cleanData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mMyAccountPresenter.getBalanceBRM()
        mMyAccountPresenter.loadInfoMyAccount()
        
    }
  
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
       
    }
    
    override func getPresenter() -> BasePresenter? {
        mMyAccountPresenter = MyAccountViewPresenter(viewController: self, myAccountDelegate: self)
        return mMyAccountPresenter
    }
    
    func cleanData(){
        mNameTitulateLabel.text = ""
        mAccountNumberLabel.text = ""
        mPackageNameLabel.text = ""
        mVelocityLabel.text = ""
        mPaymentAmountLabel.text = ""
    }
    
    func onSuccessLoadMyAccount(myAccountResponse: MyAccountResponse) {
        if myAccountResponse.information != nil {
            mNameTitulateLabel.text = myAccountResponse.information.titulate
            mAccountNumberLabel.text = myAccountResponse.information.accountNumber
            mPackageNameLabel.text = myAccountResponse.information.contractedPackage
            mVelocityLabel.text = "\(myAccountResponse.information.packageSpeed) Mbps"
            print(myAccountResponse.information.balance)
            //mPaymentAmountLabel.text = String.init(format: "$ %@", myAccountResponse.information.balance)
        }
    }

    func onSuccesCheckBalanceBRM(checkBRMRespnse: CheckBalanceBRMResponse) {
        //print(myAccountResponse.information.balance)
        let double_price = checkBRMRespnse.arrResult?.arrDatosFactura?.totalPayable != nil ? Double(checkBRMRespnse.arrResult!.arrDatosFactura!.totalPayable!) : 0.0
        mPaymentAmountLabel.text =  "$\(String(format: "%.2f", double_price!))"
    }
    @IBAction func onConsultingStatePaymentsClick(_ sender: Any) {
        mMyAccountPresenter.showStatements()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
